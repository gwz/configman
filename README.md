Chef-solo for Debian 7.1 with Nginx/webapp
====



1. Fire up a Debian 7.1 Wheezy AMI (ami-7b059641)
    
2. Fetch and kickoff install of chef-solo:

    ssh -i ~/.ssh/amazon-aws.pem admin@amazon-host 'wget --no-check-certificate https://bitbucket.org/gwz/configman/downloads/chef-solo-atron_31012014.tar.gz && tar -xfz chef-solo-atron_31012014.tar.gz && cd chef-solo-atron && sudo ./install.sh'

3. Wait about ~10min and check Nginx/webapp 
    


Assumptions
-----

* Your AMI host has wget installed
