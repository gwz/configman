#!/bin/bash

#
# Check if this is a fresh host
# Update pkgs, install ruby and gems if true
#

chef_binary=/usr/local/bin/chef-solo

if ! test -f "$chef_binary"; then
 apt-get update && 
 apt-get install -y ruby1.9.1 ruby1.9.1-dev make &&
 gem1.9.1 install chef --no-ri --no-rdoc &&
 gem1.9.1 install bundle --no-ri --no-rdoc &&
 gem1.9.1 install ruby-shadow --no-ri --no-rdoc

fi &&

"$chef_binary" -c solo.rb -j solo.json
