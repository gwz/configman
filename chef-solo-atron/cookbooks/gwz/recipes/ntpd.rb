#
# ntp configuration to disable daemon ipv6 capabilities
# 


#------------------------------------------------------------#
# ntp
#------------------------------------------------------------#
package "ntp" do
  action :install
end

cookbook_file "/etc/default/ntp" do
  source "ntp"
  owner "root"
  group "root"
  mode "0644"
  notifies :restart, "service[ntp]"
end


cookbook_file "/etc/ntp.conf" do
  source "ntp.conf"
  owner "root"
  group "root"
  mode "0644"
  notifies :restart, "service[ntp]"
end

cookbook_file "/etc/timezone" do
  source "timezone"
  owner "root"
  group "root"
  mode "0644"
end

service "ntp" do
  supports :status => true, :restart => true
  action [ :enable, :restart ]
end
