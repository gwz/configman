#
# Manage sshd configuration
#


#------------------------------------------------------------#
# sshd
#------------------------------------------------------------#
package "openssh-server" do
  action :install
end

cookbook_file "/etc/ssh/sshd_config" do
  source "sshd_config"
  owner "root"
  group "root"
  mode "0644"
  notifies :restart, "service[ssh]"
end

service "ssh" do
  supports :status => true, :restart => true
  action [ :enable, :restart ]
end
