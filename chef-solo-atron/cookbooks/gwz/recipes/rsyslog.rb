#
# Manage rsyslog configuration
#


#------------------------------------------------------------#
# rsyslog
#------------------------------------------------------------#
package "rsyslog" do
  action :install
end

cookbook_file "/etc/rsyslog.conf" do
  source "rsyslog.conf"
  owner "root"
  group "root"
  mode "0644"
  notifies :restart, "service[rsyslog]"
end

service "rsyslog" do
  supports :status => true, :restart => true, :reload => true
  action [ :enable ]
end
