#
# The prestart cleanup before any further config
#


#------------------------------------------------------------#
# Uninstall pkgs
#------------------------------------------------------------#
packages = %w[at portmap]
packages.each do |p|
  apt_package p do
    action :remove
  end
end


#------------------------------------------------------------#
# Cleanup
#------------------------------------------------------------#
execute "aptget-autoclean" do
  command "apt-get auotclean"
  action :nothing
end


#------------------------------------------------------------#
# Users/Groups
#------------------------------------------------------------#
user "gnats" do
  action :remove
end
