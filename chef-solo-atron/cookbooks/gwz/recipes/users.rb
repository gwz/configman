#
# Define users and groups
# 
# Follow the uid/gid allocations
#


#------------------------------------------------------------#
# Users/Groups
#------------------------------------------------------------#

group "slukic" do
  action :create
  gid 2000
end

user "slukic" do
  comment "slukic"
  uid 2000
  gid "slukic"
  home "/home/slukic"
  shell "/bin/bash"
  password "$6$.gG5YRPb$fi0wk58CgvPdDNZ3.PDwPGwKMYY530i9RZob.Y5bYDjA1F0Sp7rXi2aQzgOan1Ewvbdu8mqCTohS4r6XreHOr."
  supports :manage_home => true
end

