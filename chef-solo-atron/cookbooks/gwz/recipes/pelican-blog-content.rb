#
# pelican blog content - application deployment
#

#------------------------------------------------------------#
# unicorn user creation for priv separation
#------------------------------------------------------------#

group "geewiz" do
  action :create
  gid 5000
end

user "geewiz" do
  comment "geewiz blog"
  uid 5000
  gid "geewiz"
  home "/dev/null"
  shell "/bin/bash"
end
 

directory "/var/www/geewiz.org" do
  owner "geewiz"
  group "geewiz"
  mode 0755
  action :create
end


#------------------------------------------------------------#
# do some ninja stuff here
#------------------------------------------------------------#

git "/var/www/geewiz.org" do
   repository "https://github.com/gwzorg/blog.git"
   user "geewiz"
   group "geewiz"
end

cookbook_file "/etc/nginx/sites-available/geewiz" do
  source "geewiz_sites-enabled"
  owner "geewiz"
  group "geewiz"
  mode 0644
end

link "/etc/nginx/sites-enabled/geewiz" do
  to "/etc/nginx/sites-available/geewiz"
end

link "/etc/nginx/sites-enabled/default" do
  action :delete
  only_if "test -L /etc/nginx/sites-enabled/default"
end

#------------------------------------------------------------#
# restart nginx
#------------------------------------------------------------#

service "nginx" do
  supports :restart => true, :status => true, :reload => true
  action [:enable, :start]
end
