#
# The function of this recipe is to install a baseline of 
# core system packages that are required to complete the
# os configuration.
#
# ie: insserv is required for chef client to stop/start services
# 


#------------------------------------------------------------#
# Install pkgs
#------------------------------------------------------------#
packages = %w[insserv screen libio-socket-ssl-perl git]
packages.each do |p|
  apt_package p do
    action :install
  end
end
