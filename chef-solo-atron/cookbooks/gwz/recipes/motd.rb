#
# Manage your funky message of the day in here
#


#------------------------------------------------------------#
# MOTD
#------------------------------------------------------------#
cookbook_file "/etc/motd" do
  source "motd"
  owner "root"
  group "root"
  mode "0644"
end

#------------------------------------------------------------#
# /etc/issue
#------------------------------------------------------------#
cookbook_file "/etc/issue" do
  source "issue"
  owner "root"
  group "root"
  mode "0644"
end
