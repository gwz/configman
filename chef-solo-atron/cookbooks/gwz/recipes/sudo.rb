#
# Apply default sudoers configuration file, whereby sudo access
# is granted to users in the sudo group
#

#------------------------------------------------------------#
# Sudeors
#------------------------------------------------------------#
cookbook_file "/etc/sudoers.d/00_slukic" do
  source "sudoers.00_slukic"
  owner "root"
  group "root"
  mode "0440"
end
