#
# Network configuration
#


#------------------------------------------------------------#
# Disable tcp wrappers; its soo 1998
#------------------------------------------------------------#
file "/etc/hosts.allow" do
  action :delete
end

file "/etc/hosts.deny" do
  action :delete
end


#------------------------------------------------------------#
# /etc/hosts
#------------------------------------------------------------#
template "/etc/hosts" do
  source "hosts.erb"
  owner "root"
  group "root"
  mode "0644"
end
