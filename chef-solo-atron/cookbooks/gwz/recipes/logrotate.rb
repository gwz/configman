#
# Manage logrotate configuration
#


#------------------------------------------------------------#
# logrotate
#------------------------------------------------------------#
package "logrotate" do
  action :install
end

cookbook_file "/etc/logrotate.conf" do
  source "logrotate.conf"
  owner "root"
  group "root"
  mode "0644"
  notifies :restart, "service[rsyslog]"
end

service "cron" do
  supports :status => true, :restart => true, :reload => true
  action [ :enable ]
end
