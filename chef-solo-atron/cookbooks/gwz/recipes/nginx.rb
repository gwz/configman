#
# nginx configuration
# 


#------------------------------------------------------------#
# 
#------------------------------------------------------------#
apt_package "nginx" do
  action :install
end

cookbook_file "/etc/nginx/nginx.conf" do
  source "nginx.conf"
  owner "root"
  group "root"
  mode 0644
  notifies :restart, "service[nginx]"
end

directory "/var/www" do
  owner "www-data"
  group "www-data"
  mode 0755
  action :create
end

service "nginx" do
  supports :status => true, :restart => true
  action [ :enable, :restart ]
end
