#
# Reboot host
#

#------------------------------------------------------------#
# Reboot host
#------------------------------------------------------------#
execute "reboot" do
  command "sudo reboot"
  action :nothing
end
