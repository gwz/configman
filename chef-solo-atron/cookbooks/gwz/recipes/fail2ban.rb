#
# fail2ban configuration for basic implementation
#


#------------------------------------------------------------#
# fail2ban
#------------------------------------------------------------#
package "fail2ban" do
  action :install
end

cookbook_file "/etc/fail2ban/fail2ban.conf" do
  source "fail2ban/fail2ban.conf"
  owner "root"
  group "root"
  mode "0644"
end
cookbook_file "/etc/fail2ban/jail.conf" do
  source "fail2ban/jail.conf"
  owner "root"
  group "root"
  mode "0644"
end
cookbook_file "/etc/fail2ban/action.d/iptables.conf" do
  source "fail2ban/f2b_iptables.conf"
  owner "root"
  group "root"
  mode "0644"
end
cookbook_file "/etc/fail2ban/action.d/iptables-multiport.conf" do
  source "fail2ban/f2b_iptables-multiport.conf"
  owner "root"
  group "root"
  mode "0644"
end
cookbook_file "/etc/fail2ban/filter.d/sshd.conf" do
  source "fail2ban/f2b_sshd.conf"
  owner "root"
  group "root"
  mode "0644"
end
cookbook_file "/etc/fail2ban/filter.d/sshd-ddos.conf" do
  source "fail2ban/f2b_sshd-ddos.conf"
  owner "root"
  group "root"
  mode "0644"
  notifies :restart, "service[fail2ban]"
end

service "fail2ban" do
  supports :status => true, :restart => true
  action [ :enable, :restart ]
end
