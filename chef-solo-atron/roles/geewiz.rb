name "geewiz"
description "geewiz pilot/staging role for basic nginx setup and application deployment"
run_list(
 "recipe[gwz::nginx]",
 "recipe[gwz::pelican-blog-content]"
)
