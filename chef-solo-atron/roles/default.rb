name "default"
description "default role that uses the following recipes on all hosts"
run_list(
 "recipe[gwz::prestart]",
 "recipe[gwz::base-pkgs]",
 "recipe[gwz::users]",
 "recipe[gwz::sudo]",
 "recipe[gwz::sshd]",
 "recipe[gwz::ntpd]",
 "recipe[gwz::network]",
 "recipe[gwz::fail2ban]",
 "recipe[gwz::rsyslog]",
 "recipe[gwz::motd]",
 "recipe[gwz::logrotate]"
)
